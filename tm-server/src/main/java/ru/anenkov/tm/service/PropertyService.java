package ru.anenkov.tm.service;

import ru.anenkov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    private final String NAME = "/application.properties";

    private final Properties properties = new Properties();

    public void init() {
        final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getServerHost() {
        final String propertyHost = this.properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public Integer getServerPort() {
        final String propertyPort = this.properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Override
    public String getSessionSalt() {
        return this.properties.getProperty("session.salt");
    }

    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(this.properties.getProperty("session.cycle"));
    }

}
