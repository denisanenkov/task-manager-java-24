package ru.anenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IRepository;
import ru.anenkov.tm.api.service.IService;
import ru.anenkov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IRepository<T> repository;

    protected AbstractService(@NotNull IRepository<T> repository) {
        this.repository = repository;
    }

    public void load(@Nullable final List<T> t) {
        if (t == null) return;
        repository.load(t);
    }

    public void load(@Nullable final T... t) {
        if (t == null) return;
        repository.load(t);
    }

    @Override
    public @Nullable List<T> getList() {
        return repository.getList();
    }

    @Override
    public void merge(@Nullable T element) {
        repository.merge(element);
    }

    @Override
    public void merge(@Nullable Collection<T> elements) {
        repository.merge(elements);
    }

    @Override
    public void merge(@Nullable T... elements) {
        repository.merge(elements);
    }

    @Override
    public void clearAll() {
        repository.clear();
    }

    void remove(T t) {
        repository.remove(t);
    }

}
