package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    List<User> findAll();

    @Nullable
    User create(String login, String password);

    @Nullable
    User create(String login, String password, String email);

    @Nullable
    User create(String login, String password, Role role);

    @Nullable
    User findById(String id);

    @Nullable
    User findByLogin(String login);

    @Nullable
    User findByEmail(String email);

    @NotNull
    User removeUser(User user);

    @NotNull
    User removeById(String id);

    @NotNull
    User removeByLogin(String login);

    @NotNull
    User removeByEmail(String email);

    @Nullable
    User updateUserFirstName(@Nullable String userId, @Nullable String newFirstName);

    @Nullable
    User updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName);

    @Nullable
    User updateUserLastName(@Nullable String userId, @Nullable String newLastName);

    @Nullable
    User updateUserEmail(@Nullable String userId, @Nullable String newEmail);

    @Nullable
    User updatePassword(@Nullable String userId, @Nullable String newPassword);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

    void deleteUserByLogin(@NotNull String login);

    void load(@Nullable List<User> users);

    void load(@Nullable User... users);

    @Nullable
    List<User> getList();

}
