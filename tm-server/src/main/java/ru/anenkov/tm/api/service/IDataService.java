package ru.anenkov.tm.api.service;

import ru.anenkov.tm.dto.Domain;

import java.io.File;

public interface IDataService {

    void saveDataBinary();

    void saveDataBase64();

    void saveDataXML();

    void saveDataJson();

    void loadDataBinary();

    void loadDataBase64();

    void loadDataXML();

    void loadDataJson();

    void clearDataBinary();

    void clearDataBase64();

    void clearDataXML();

    void clearDataJson();

    Domain returnDomain();

    File returnFile(String nameFile);

    void clearFile(String pathFile);

}
