package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminEndpoint {

    @WebMethod
    void clearDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception;

    @WebMethod
    void loadDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception;

    @WebMethod
    void saveDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception;

    @WebMethod
    void clearDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void loadDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void saveDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void cleanDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void loadDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void saveDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void clearDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void loadDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void saveDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );


}
