package ru.anenkov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.exception.AbstractException;

public class EmptySignatureSessionException extends AbstractException {

    public EmptySignatureSessionException() {
        super("Error! Session signature is empty!");
    }

}
