package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyUserIdSessionException extends AbstractException {

    public EmptyUserIdSessionException() {
        super("Error! User Id in Session is Empty!");
    }

}
