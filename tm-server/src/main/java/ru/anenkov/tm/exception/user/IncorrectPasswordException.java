package ru.anenkov.tm.exception.user;

import ru.anenkov.tm.exception.AbstractException;

public class IncorrectPasswordException extends AbstractException {

    public IncorrectPasswordException() {
        super("Error! Password is incorrect...");
    }

}
