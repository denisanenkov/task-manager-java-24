package ru.anenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IUserEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public UserEndpoint() {
        super(null);
    }

    @WebMethod
    @Override
    @Nullable
    public List<User> findAllUser(
            @NotNull @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    @Override
    @Nullable
    public User createUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password);
    }

    @WebMethod
    @Override
    @Nullable
    public User findByIdUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(id);
    }

    @WebMethod
    @Override
    @Nullable
    public User findByLoginUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    @Override
    @Nullable
    public User findByEmailUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "email") String email
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByEmail(email);
    }

    @WebMethod
    @Override
    @Nullable
    public User updateUserFirstName(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "newFirstName") String newFirstName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserFirstName(session.getUserId(), newFirstName);
    }

    @WebMethod
    @Override
    @Nullable
    public User updateUserMiddleName(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "newMiddleName") String newMiddleName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserMiddleName(session.getUserId(), newMiddleName);
    }

    @WebMethod
    @Override
    @Nullable
    public User updateUserLastName(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "newLastName") String newLastName
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserLastName(session.getUserId(), newLastName);
    }

    @WebMethod
    @Override
    @Nullable
    public User updateUserEmail(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "newEmail") String newEmail
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateUserEmail(session.getUserId(), newEmail);
    }

    @WebMethod
    @Override
    @Nullable
    public User updatePasswordUser(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "newPassword") String newPassword
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updatePassword(session.getUserId(), newPassword);
    }

    @WebMethod
    @Override
    @Nullable
    public List<User> getListUser(
            @NotNull @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().getList();
    }

    @WebMethod
    @Override
    @Nullable
    public User showUserProfile(
            @NotNull @WebParam(name = "session") final Session session
    ) {
        User user = serviceLocator.getAuthService().showUserProfile();
        return user;
    }

    @WebMethod
    @Nullable
    public void checkRoleUser(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "roles") final Role[] roles
    ) {
        serviceLocator.getAuthService().checkRole(roles);
    }

    @WebMethod
    @Nullable
    public void findById(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "roles") final Role[] roles
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().findById(session.getUserId());
    }

}