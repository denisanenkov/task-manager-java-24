package ru.anenkov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.anenkov.tm.entity.Session;

public final class SignatureUtil {

    public static String sign(
            final Object value,
            final String salt,
            final Integer cycle
    ) {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static String sign(
            String value,
            String salt,
            Integer cycle
    ) {
        if (value == null || salt == null) return null;
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.MD5(salt + result + salt);
        }
        return result;
    }

    public static void main(String[] args) throws JsonProcessingException {
        Session sessions = new Session();
        sessions.setId("111");
        sessions.setUserId("124");
        sessions.setTimestamp(System.currentTimeMillis());

        System.out.println(new ObjectMapper().writeValueAsString(sessions));
        System.out.println(sign(sessions, "MEGA", 2234));
        System.out.println(sign(sessions, "MEGA", 2234));
    }

}
