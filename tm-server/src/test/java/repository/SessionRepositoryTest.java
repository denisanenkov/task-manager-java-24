package repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.repository.SessionRepository;
import ru.anenkov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class SessionRepositoryTest {

    final SessionRepository sessionRepository = new SessionRepository();
    public User user = new User();

    @Before
    public void beforeTest() {
        user.setRole(Role.USER);
        user.setFirstName("Denis");
        user.setLastName("Anenkov");
        user.setMiddleName("Aleksandrovich");
        user.setEmail("denk.an@inbox.ru");
        user.setLogin("test");
        user.setPasswordHash("passTest");
    }

    @Test
    public void createTest() {
        Session session = new Session(user.getId());
        Assert.assertNull(session.getSignature());
        Assert.assertNull(session.getTimestamp());
        session.setSignature("signature");
        session.setId("IdSession");
        session.setTimestamp(1L);
        Assert.assertNotNull(session.getSignature());
        Assert.assertNotNull(session.getTimestamp());
    }

    @Test
    public void addTest() {
        Session session = new Session(user.getId());
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.merge(session);
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void findTest() {
        Session session = new Session(user.getId());
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.merge(session);
        List<Session> session1 = new ArrayList<>();
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        Assert.assertTrue(session1.isEmpty());
        session1 = sessionRepository.findByUserId(user.getId());
        Assert.assertFalse(session1.isEmpty());
    }

    @Test
    public void removeTest() {
        Session session = new Session(user.getId());
        sessionRepository.merge(session);
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        sessionRepository.removeByUserId(user.getId());
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void findAllTest() {
        Session session = new Session();
        Session session1 = new Session();
        Session session2 = new Session();
        List<Session> sessions = new ArrayList<>();
        sessions.add(session);
        sessions.add(session1);
        sessions.add(session2);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.merge(sessions);
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
    }

}
