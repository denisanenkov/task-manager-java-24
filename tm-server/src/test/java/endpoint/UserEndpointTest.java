package endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;

public class UserEndpointTest {

    final Bootstrap bootstrap = new Bootstrap();
    final UserEndpoint userEndpoint = new UserEndpoint(bootstrap);
    final SessionEndpoint sessionEndpoint = new SessionEndpoint(bootstrap);

    @Before
    public void before() {
        bootstrap.initUsers();
    }

    @Test
    public void createUserTest() {
        final Session session = sessionEndpoint.openSession("1","1");
        Assert.assertEquals(userEndpoint.getListUser(session).size(), 4);
        final User user = userEndpoint.createUser(session, "loginNewUser", "passNewUser");
        Assert.assertEquals(userEndpoint.getListUser(session).size(), 5);
    }

    @Test
    public void findByParameters() {
        final Session session = sessionEndpoint.openSession("1","1");
        final User user = userEndpoint.createUser(session, "loginNewUser", "passNewUser");
        user.setEmail("test@mail.ru");
        User user1 = null, user2 = null, user3 = null;
        Assert.assertNull(user1);
        Assert.assertNull(user2);
        Assert.assertNull(user3);
        user1 = userEndpoint.findByIdUser(session, user.getId());
        user2 = userEndpoint.findByEmailUser(session, user.getEmail());
        user3 = userEndpoint.findByLoginUser(session, user.getLogin());
        Assert.assertEquals(user1.getId(), user.getId());
        Assert.assertEquals(user1.getEmail(), user.getEmail());
        Assert.assertEquals(user1.getLogin(), user.getLogin());
    }

    @Test
    public void updateUserParameters() {
        final Session session = sessionEndpoint.openSession("1","1");
        final User user = userEndpoint.createUser(session, "loginNewUser", "passNewUser");
        user.setEmail("test@mail.ru");
        user.setFirstName("Denis");
        user.setMiddleName("Alexandrovich");
        user.setLastName("Anenkov");
        final Session newSession = sessionEndpoint.openSession("loginNewUser","passNewUser");
        userEndpoint.updateUserFirstName(newSession, "newName");
        Assert.assertEquals("newName", user.getFirstName());
        userEndpoint.updateUserMiddleName(newSession, "newMiddleName");
        Assert.assertEquals("newMiddleName", user.getMiddleName());
        userEndpoint.updateUserLastName(newSession, "NewLastName");
        Assert.assertEquals("NewLastName", user.getLastName());
    }

}
