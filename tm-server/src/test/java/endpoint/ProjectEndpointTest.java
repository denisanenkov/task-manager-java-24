package endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class ProjectEndpointTest {

    final Bootstrap bootstrap = new Bootstrap();
    final ProjectEndpoint projectEndpoint = new ProjectEndpoint(bootstrap);
    final SessionEndpoint sessionEndpoint = new SessionEndpoint(bootstrap);

    @Before
    public void before() {
        bootstrap.initUsers();
    }

    @Test
    public void createTest() {
        final Session session = sessionEndpoint.openSession("1","1");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 0);
        projectEndpoint.createProject(session, "projectName", "projectDescription");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 1);
    }

    @Test
    public void clearTasks() {
        final Session session = sessionEndpoint.openSession("1","1");
        projectEndpoint.createProject(session, "projectName1", "projectDescription1");
        projectEndpoint.createProject(session, "projectName2", "projectDescription2");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 2);
        projectEndpoint.clear(session);
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 0);
    }

    @Test
    public void findByParameters() {
        final Session session = sessionEndpoint.openSession("1","1");
        projectEndpoint.createProject(session, "projectName1", "projectDescription1");
        projectEndpoint.createProject(session, "projectName2", "projectDescription2");
        projectEndpoint.createProject(session, "projectName3", "projectDescription3");
        Project project1 = null, project2 = null, project3 = null;
        Assert.assertNull(project1);
        project1 = projectEndpoint.findOneByNameProject(session, "projectName1");
        Assert.assertNotNull(project1);
        Assert.assertNull(project2);
        project2 = projectEndpoint.findOneByIndexProject(session, 1);
        Assert.assertNotNull(project2);
        Assert.assertNull(project3);
        Project project = projectEndpoint.findOneByNameProject(session, "projectName3");
        project3 = projectEndpoint.findOneByIdProject(session, project.getId());
        Assert.assertNotNull(project3);
        Assert.assertEquals(project1.getName(), "projectName1");
        Assert.assertEquals(project2.getDescription(), "projectDescription2");
        Assert.assertEquals(project3.getName(), "projectName3");
    }

    @Test
    public void updateByParameters() {
        final Session session = sessionEndpoint.openSession("1","1");
        projectEndpoint.createProject(session, "projectName1", "projectDescription1");
        projectEndpoint.createProject(session, "projectName2", "projectDescription2");
        Project project1 = projectEndpoint.findOneByNameProject(session, "projectName1");
        Project project2 = projectEndpoint.findOneByNameProject(session, "projectName2");
        Assert.assertEquals(project1.getName(), "projectName1");
        Assert.assertEquals(project2.getName(), "projectName2");
        projectEndpoint.updateByIndexProject(session, 0, "newName1", "new Description1");
        projectEndpoint.updateByIdProject(session, project2.getId(), "newName2", "new Description2");
        Assert.assertEquals(project1.getName(), "newName1");
        Assert.assertEquals(project2.getName(), "newName2");
        Assert.assertEquals(project1.getDescription(), "new Description1");
        Assert.assertEquals(project2.getDescription(), "new Description2");
    }

    @Test
    public void removeByParameter() {
        final Session session = sessionEndpoint.openSession("1","1");
        Assert.assertTrue(projectEndpoint.findAllProjects(session).isEmpty());
        projectEndpoint.createProject(session, "projectName1", "projectDescription1");
        projectEndpoint.createProject(session, "projectName2", "projectDescription2");
        projectEndpoint.createProject(session, "projectName3", "projectDescription3");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 3);
        Project projectTask = projectEndpoint.findOneByNameProject(session, "projectName2");
        projectEndpoint.removeOneByNameProject(session, "projectName1");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 2);
        projectEndpoint.removeOneByIdProject(session, projectTask.getId());
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 1);
        projectEndpoint.removeOneByIndexProject(session, 0);
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 0);
    }

    @Test
    public void getListTest() {
        final Session session = sessionEndpoint.openSession("1","1");
        projectEndpoint.createProject(session, "projectName1", "projectDescription1");
        projectEndpoint.createProject(session, "projectName2", "projectDescription2");
        projectEndpoint.createProject(session, "projectName3", "projectDescription3");
        List<Project> projectList = new ArrayList<>();
        Assert.assertTrue(projectList.isEmpty());
        Assert.assertEquals(projectList.size(), 0);
        projectList = projectEndpoint.getListProjects(session);
        Assert.assertFalse(projectList.isEmpty());
        Assert.assertEquals(projectList.size(), 3);
    }

}
