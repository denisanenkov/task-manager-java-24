package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import service.ProjectServiceTest;
import service.SessionServiceTest;
import service.TaskServiceTest;
import service.UserServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProjectServiceTest.class,
        SessionServiceTest.class,
        TaskServiceTest.class,
        UserServiceTest.class})
public class ServiceSuite {
}
