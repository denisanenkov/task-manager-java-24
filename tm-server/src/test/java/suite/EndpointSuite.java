package suite;

import endpoint.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AdminUserEndpointTest.class,
        ProjectEndpointTest.class,
        SessionEndpointTest.class,
        TaskEndpointTest.class,
        UserEndpointTest.class})
public class EndpointSuite {
}
