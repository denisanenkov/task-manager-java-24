package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import repository.ProjectRepositoryTest;
import repository.SessionRepositoryTest;
import repository.TaskRepositoryTest;
import repository.UserRepositoryTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProjectRepositoryTest.class,
        SessionRepositoryTest.class,
        TaskRepositoryTest.class,
        UserRepositoryTest.class})
public class RepositorySuite {
}
