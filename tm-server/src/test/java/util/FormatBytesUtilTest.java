package util;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.util.NumberUtil;

public class FormatBytesUtilTest {

    @Test
    public void formatBytesTest() {
        Assert.assertEquals(NumberUtil.formatBytes(1023), "1023 B");
        Assert.assertEquals(NumberUtil.formatBytes(1024), "1 KB");
        Assert.assertEquals(NumberUtil.formatBytes(2048), "2 KB");
        Assert.assertEquals(NumberUtil.formatBytes(1_234_567), "1 MB");
        Assert.assertEquals(NumberUtil.formatBytes(1_234_567_890), "1 GB");
        Assert.assertEquals(NumberUtil.formatBytes(2_000_000_000_000L), "1 TB");
        Assert.assertEquals(NumberUtil.formatBytes(10_000_000_000_000L), "9 TB");
    }

}
