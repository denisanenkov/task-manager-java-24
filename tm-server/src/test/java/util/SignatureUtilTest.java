package util;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.util.SignatureUtil;

public class SignatureUtilTest {

    @Test
    public void signatureUtilTest() {
        Session sessions = new Session();
        sessions.setId("111");
        sessions.setUserId("124");
        sessions.setTimestamp(System.currentTimeMillis());
        Assert.assertNull(sessions.getSignature());
        String signature = "";
        Assert.assertEquals(signature, "");
        signature = SignatureUtil.sign(sessions, "MEGA", 2234);
        Assert.assertNotEquals(signature, "");
        Assert.assertNull(sessions.getSignature());
        sessions.setSignature(signature);
        Assert.assertNotNull(sessions.getSignature());
    }

}
