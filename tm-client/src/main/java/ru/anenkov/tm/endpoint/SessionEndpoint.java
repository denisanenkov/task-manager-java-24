package ru.anenkov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-18T00:32:17.782+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.tm.anenkov.ru/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/SessionEndpoint/openSessionRequest", output = "http://endpoint.tm.anenkov.ru/SessionEndpoint/openSessionResponse")
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    ru.anenkov.tm.endpoint.Session openSession(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/SessionEndpoint/closeSessionAllRequest", output = "http://endpoint.tm.anenkov.ru/SessionEndpoint/closeSessionAllResponse")
    @RequestWrapper(localName = "closeSessionAll", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CloseSessionAll")
    @ResponseWrapper(localName = "closeSessionAllResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CloseSessionAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    ru.anenkov.tm.endpoint.Result closeSessionAll(
            @WebParam(name = "session", targetNamespace = "")
                    ru.anenkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/SessionEndpoint/clearAllSessionsRequest", output = "http://endpoint.tm.anenkov.ru/SessionEndpoint/clearAllSessionsResponse")
    @RequestWrapper(localName = "clearAllSessions", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.ClearAllSessions")
    @ResponseWrapper(localName = "clearAllSessionsResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.ClearAllSessionsResponse")
    void clearAllSessions();

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/SessionEndpoint/closeSessionRequest", output = "http://endpoint.tm.anenkov.ru/SessionEndpoint/closeSessionResponse")
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.CloseSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    ru.anenkov.tm.endpoint.Result closeSession(
            @WebParam(name = "session", targetNamespace = "")
                    ru.anenkov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.anenkov.ru/SessionEndpoint/allSessionsRequest", output = "http://endpoint.tm.anenkov.ru/SessionEndpoint/allSessionsResponse")
    @RequestWrapper(localName = "allSessions", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.AllSessions")
    @ResponseWrapper(localName = "allSessionsResponse", targetNamespace = "http://endpoint.tm.anenkov.ru/", className = "ru.anenkov.tm.endpoint.AllSessionsResponse")
    @WebResult(name = "return", targetNamespace = "")
    java.util.List<ru.anenkov.tm.endpoint.Session> allSessions(
            @WebParam(name = "session", targetNamespace = "")
                    ru.anenkov.tm.endpoint.Session session
    );
}
