package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Project;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectFindByIndexClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Show-project-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Show project by index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = bootstrap.getProjectEndpoint().findOneByIndexProject(bootstrap.getSession(), index);
        if (project == null) return;
        System.out.println("" +
                "NAME: " + project.getName() +
                ", \nDESCRIPTION: " + project.getDescription() +
                ", \nUSER ID: " + project.getUserId() +
                ", \nTASK ID: " + project.getId()
        );
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
