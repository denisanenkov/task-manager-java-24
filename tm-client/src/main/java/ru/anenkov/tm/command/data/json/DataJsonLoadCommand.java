package ru.anenkov.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

public class DataJsonLoadCommand extends AbstractCommandClient {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Data-json-load";
    }

    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA JSON LOAD]");
        bootstrap.getAdminEndpoint().loadDataJson(bootstrap.getSession());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
