package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;

public class ExitApplicationClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-e";
    }

    @Override
    public @Nullable String name() {
        return "Exit";
    }

    @Override
    public @Nullable String description() {
        return "Exit application";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SYSTEM EXIT]");
        System.exit(0);
    }

}
