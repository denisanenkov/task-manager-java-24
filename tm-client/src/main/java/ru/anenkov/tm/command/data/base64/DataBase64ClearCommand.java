package ru.anenkov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

public class DataBase64ClearCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        bootstrap.getAdminEndpoint().clearDataBase64(bootstrap.getSession());
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

