package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.command.AbstractCommandClient;

public class AboutDevClientCommand extends AbstractCommandClient {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "About";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Denis Anenkov");
        System.out.println("E-MAIL: denk.an@inbox.ru");
    }

}
