import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.marker.*;

@Category(AllCategory.class)
public class TaskEndpointTest {

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Test
    @Category(CreateCategory.class)
    public void createTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        int numOfTasks = taskEndpoint.findAll(session).size();
        Assert.assertEquals(taskEndpoint.findAll(session).size(), numOfTasks);
        taskEndpoint.create(session, "first", "description");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), numOfTasks + 1);
    }

    @Test
    @Category(SearchCategory.class)
    public void findTasksByParametersTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.clear(session);
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        taskEndpoint.create(session, "first", "description");
        taskEndpoint.create(session, "second", "description");
        taskEndpoint.create(session, "third", "description");
        Task task = null;
        Assert.assertNull(task);
        task = taskEndpoint.findOneByIndex(session, 1);
        Assert.assertNotNull(task);
        task = null;
        Assert.assertNull(task);
        task = taskEndpoint.findOneByName(session, "first");
        Assert.assertNotNull(task);
        Task testTask = taskEndpoint.findOneByName(session, "third");
        task = null;
        Assert.assertNull(task);
        task = taskEndpoint.findOneById(session, testTask.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), "third");
    }

    @Test
    @Category(UpdateCategory.class)
    public void addTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.clear(session);
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        Task task1 = new Task();
        task1.setUserId(session.getUserId());
        task1.setName("name");
        task1.setDescription("description");
        task1.setId("1");
        taskEndpoint.add(session, task1);
        Assert.assertFalse(taskEndpoint.findAll(session).isEmpty());
    }

    @Test
    @Category(RemoveCategory.class)
    public void removeTasksTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.clear(session);
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        taskEndpoint.create(session, "first", "description");
        taskEndpoint.create(session, "second", "description");
        taskEndpoint.create(session, "third", "description");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 3);
        taskEndpoint.removeOneByName(session, "first");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 2);
        taskEndpoint.removeOneByIndex(session, 0);
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 1);
        Task task = taskEndpoint.findOneByName(session, "third");
        taskEndpoint.removeOneById(session, task.getId());
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 0);
    }

    @Test
    @Category(UpdateCategory.class)
    public void updateTaskParametersTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        taskEndpoint.clear(session);
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        taskEndpoint.create(session, "first", "description");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 1);
        taskEndpoint.updateTaskByIndex(session, 0, "newName", "newDescription");
        Assert.assertEquals(taskEndpoint.findOneByIndex(session, 0).getName(), "newName");
        taskEndpoint.updateTaskById(session, taskEndpoint.findOneByIndex(session, 0).getId(), "newName2", "newnew");
        Assert.assertEquals(taskEndpoint.findOneByIndex(session, 0).getName(), "newName2");
    }

}