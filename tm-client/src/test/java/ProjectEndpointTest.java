import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.marker.*;

@Category(AllCategory.class)
public class ProjectEndpointTest {

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Test
    @Category(CreateCategory.class)
    public void createProjectTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        int numOfTasks = projectEndpoint.findAllProjects(session).size();
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), numOfTasks);
        projectEndpoint.createProject(session, "first", "description");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), numOfTasks + 1);
    }

    @Test
    @Category(SearchCategory.class)
    public void findProjectsByParametersTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        projectEndpoint.clear(session);
        Assert.assertTrue(projectEndpoint.findAllProjects(session).isEmpty());
        projectEndpoint.createProject(session, "first", "description");
        projectEndpoint.createProject(session, "second", "description");
        projectEndpoint.createProject(session, "third", "description");
        Project project = null;
        Assert.assertNull(project);
        project = projectEndpoint.findOneByIndexProject(session, 1);
        Assert.assertNotNull(project);
        project = null;
        Assert.assertNull(project);
        project = projectEndpoint.findOneByNameProject(session, "first");
        Assert.assertNotNull(project);
        Project testTask = projectEndpoint.findOneByNameProject(session, "third");
        project = null;
        Assert.assertNull(project);
        project = projectEndpoint.findOneByIdProject(session, testTask.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), "third");
    }

    @Test
    @Category(UpdateCategory.class)
    public void addProjectTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        projectEndpoint.clear(session);
        Assert.assertTrue(projectEndpoint.findAllProjects(session).isEmpty());
        Project project1 = new Project();
        project1.setUserId(session.getUserId());
        project1.setName("name");
        project1.setDescription("description");
        project1.setId("1");
        projectEndpoint.addProject(session, project1);
        Assert.assertFalse(projectEndpoint.findAllProjects(session).isEmpty());
    }

    @Test
    @Category(RemoveCategory.class)
    public void removeProjectsTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        projectEndpoint.clear(session);
        Assert.assertTrue(projectEndpoint.findAllProjects(session).isEmpty());
        projectEndpoint.createProject(session, "first", "description");
        projectEndpoint.createProject(session, "second", "description");
        projectEndpoint.createProject(session, "third", "description");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 3);
        projectEndpoint.removeOneByNameProject(session, "first");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 2);
        projectEndpoint.removeOneByIndexProject(session, 0);
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 1);
        Project project = projectEndpoint.findOneByNameProject(session, "third");
        projectEndpoint.removeOneByIdProject(session, project.getId());
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 0);
    }

    @Test
    @Category(UpdateCategory.class)
    public void updateProjectParametersTest() {
        final Session session = sessionEndpoint.openSession("test", "test");
        projectEndpoint.clear(session);
        Assert.assertTrue(projectEndpoint.findAllProjects(session).isEmpty());
        projectEndpoint.createProject(session, "first", "description");
        Assert.assertEquals(projectEndpoint.findAllProjects(session).size(), 1);
        projectEndpoint.updateByIndexProject(session, 0, "newName", "newDescription");
        Assert.assertEquals(projectEndpoint.findOneByIndexProject(session, 0).getName(), "newName");
        projectEndpoint.updateByIdProject(session, projectEndpoint.findOneByIndexProject(session, 0).getId(), "newName2", "newnew");
        Assert.assertEquals(projectEndpoint.findOneByIndexProject(session, 0).getName(), "newName2");
    }

}
